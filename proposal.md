## About Me

Hi, I am a Senior Engineering student at Pakistan Institute of Engineering and Applied Sciences, Islamabad. I have been into computer programming since 7th grade and I plan to build a career as a Software Developer. I have explored various domains of Software Development such as mobile, desktop, web (full-stack) and scientific computation.

### Personal Details
- **Name**: Moazin Khatti
- **Email**: moazinkhatri@gmail.com
- **Github, Gitlab, IRC**: @moazin

I am applying to FreeType this year for the project **Reimplementation of the font-rs rendering engine in FreeType**.

# Reimplementation of the font-rs rendering engine in FreeType

## Background
FreeType is a popular font rasterization library. It can do two things:
1. Extracting information from font files. This includes a lot of things amongst which the most important ones are glyph outlines, which are mathematical curves which describe a glyph.
2. Render those glyphs into bitmaps.

To do the latter, FreeType has some rendering modules in it. One for monochrome rendering and another one for anti-aliased rendering (with 256 levels of gray).
[font-rs](https://github.com/raphlinus/font-rs) is a recent font rendering engine written in Rust that performs faster than FreeType's anti-aliasing renderer. The goal of this project is to rewrite the renderer in C and integrate it with FreeType. Ultimately as it matures, it might even replace the old one.

## Proposed Timeline
GSoC's coding period is roughly 12 weeks and the community bonding period's duration is a month. I have organized the work in the following manner:
1. **Community Bonding Period**
    * Understand the rasterization algorithm of **font-rs**. Since the code is very clean and the rasterization part is actually small (under 200 lines of code), I hope to do it within a week. If time allows, I would also want to create a document explaining the algorithm with illustrations. 
    * Creating a very simple testing program that can compare rendering outputs. This would be used later when I reimplement the rasterizer in C to compare its results with the original Rust implementation. The program will be a simple C program that could use FreeType to read glyph outlines from font files and then render them using both engines, ultimately comparing their rendered bitmaps.
    * Closely studying the differences between FreeType and font-rs in terms of how they handle data. These would be useful when integrating the C implementation in FreeType as a rendering module. Some that I have already found are:
        * FreeType stores scaled outlines with coordinates being in the fixed point format *26.6*. **Font-rs** stores these as floating points.
        * The main rasterization algorithm of **font-rs** takes the *y* coordinate of the points as distances from the `ymax` of the scaled bounding box of a glyph, not as an absolute coordinate w.r.t the origin. This conversion happens when coordinates are multiplied with an **affine** transformation matrix. The same matrix also scales the glyph to the required size. In FreeType, `FT_Load_Glyph` already scales the outlines, so ultimately the rendering module would receive scaled glyphs and some code would be needed to transform the coordinates to the format that the rendering algorithm expects. More will be learnt when I take a closer look at the actual rasterization code.
    * Decide on an algorithm to convert cubic beziers to quadratic/linear curves and write C code for it.
    * Explore the SIMD accumulation code in font-rs along with exploring things such as:
        * How to detect SIMD availibility on *x86_64* and *ARM v8* systems?
        * What are the necessary build (compilation) steps for running SIMD code in C?
2. **Week 1-2**: Implement the main rasterization algorithms found in `raster.rs` of **font-rs** in C. Test them using the testing program prepared during the community bonding period. It should be ensured that the two engines give exactly identical results. A performance comparison would be interesting to do. (Though I don't expect much difference)
3. **Week 3**: Bring in the cubic to quadratic/linear algorithm in the engine to be able to handle outlines which have cubic bezier curves. If for any reason this causes a lot of problems that can't be managed in a week, it should be skipped for later.
4. **Week 4-5**: Create a new rendering module in FreeType and integrate the newly written C code there. It would require writing some conversion code that can transform the data that the glyphslot has to a format that the rasterization algorithm expects (this would most likely consist of some arithmetic operations). Some configuration macro would be created to let the users choose which rendering engine to use.
5. **Week 6**: The conversions mentioned above won't be computationally expensive (I hope), but would still become a factor. It should be analyzed how the algorithm (or some code on FreeType side) can be changed to minimze such conversions. At this point it would be nice to do some basic testing to ensure everything is working great.
6. **Week 7-9**: Integrating the SIMD based accumulation algorithm in FreeType and ensuring that it runs on *x86_64* and *ARM v8*.
7. **Week 10**: Testing the newly integrated engine with multiple fonts. A performance test with FreeType's `smooth` rasterizer should be done. This would really be a good measure of the "success" of this project.
8. **Week 11-12**: These are buffer weeks at the moment. These can be spent for doing anything that would be left. Sufficient amount of time would be spent to ensure that the code is well documented.


## Possible Timing Constraints
According to my original semester schedule, I would have had some exams overlapping with the GSoC period (roughly 2-3 weeks). However, due to COVID-19, the country is currently under a lockdown and we have no idea how the university will progress. It might remain off for a long time. What I can say for sure is that I'll give this project as much time as it requires. :-)
